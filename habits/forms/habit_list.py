from django import forms
from django.forms import TextInput


class SearchHabitForm(forms.Form):
    search = forms.CharField(
        required=False,
        widget=TextInput(attrs={'placeholder': 'Поиск...'})
    )
