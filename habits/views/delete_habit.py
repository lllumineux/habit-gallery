from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect

from habits.models import Habit


@login_required(redirect_field_name='', login_url='/login/')
def delete_habit_view(request, pk=None):
    habit = get_object_or_404(Habit, pk=pk, author=request.user)
    habit.delete()
    return redirect('habit-list')
