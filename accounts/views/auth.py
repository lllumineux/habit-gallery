from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect

from accounts.forms import SignupForm, LoginForm
from accounts.utils import not_authenticated_required, create_user


@not_authenticated_required
def signup_view(request):
    form = SignupForm()

    if request.method == 'POST':
        form = SignupForm(request.POST)

        if form.is_valid():
            user = create_user(form.cleaned_data['email'], form.cleaned_data['password'])
            login(request, user)
            return redirect('habit-list')

    return render(request, 'accounts/signup.html', locals())


@not_authenticated_required
def login_view(request):
    form = LoginForm()

    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(
                request,
                username=form.cleaned_data['email'],
                password=form.cleaned_data['password']
            )
            if user is not None:
                login(request, user)
                return redirect('habit-current-list')
            form.add_error('email', 'Неверный email или пароль')
            form.add_error('password', 'Неверный email или пароль')

    return render(request, 'accounts/login.html', locals())


def logout_view(request):
    logout(request)
    return redirect('login')
