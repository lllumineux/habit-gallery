from django.db import models

from accounts.models import User
from habits.choices import HabitTypes, HabitPaintingTypes, HabitDeadlineCheckStatus

NULLABLE = {'blank': True, 'null': True}


class Habit(models.Model):
    type = models.CharField(
        verbose_name='Тип',
        max_length=15,
        choices=HabitTypes.choices,
        default=HabitTypes.DAILY
    )
    name = models.CharField(
        verbose_name='Название',
        max_length=255
    )
    start_date = models.DateField(
        verbose_name='Дата начала'
    )
    frequency = models.PositiveIntegerField(
        verbose_name='Частота',
        help_text='Каждые n дней/недель/месяцев',
        default=1
    )
    required_amount = models.PositiveIntegerField(
        verbose_name='Нужное количество',
        help_text='За 1 день/неделю/месяц',
        default=1
    )

    author = models.ForeignKey(
        User,
        verbose_name='Автор',
        on_delete=models.CASCADE
    )
    completed_amount = models.PositiveIntegerField(
        verbose_name='Выполненное количество',
        default=0
    )
    current_streak = models.PositiveIntegerField(
        verbose_name='Текущее значение стрика',
        default=0
    )

    # If type is either 'weekly' or 'monthly'
    selected_days = models.JSONField(
        verbose_name='Выбранные дни',
        help_text='Дни недели / Числа месяца',
        default=list
    )
    required_amount_per_day = models.PositiveIntegerField(
        verbose_name='Нужное количество в день',
        default=1
    )

    @property
    def current_required_amount(self):
        if self.type == 'daily' or not self.selected_days:
            return self.required_amount
        else:
            return self.required_amount_per_day

    @property
    def complete_percentage(self):
        return min(round(self.completed_amount / self.current_required_amount * 100), 100)

    @property
    def painting_cnt(self):
        return (
            self.habitpainting_set
            .filter(type__in=[
                HabitPaintingTypes.PARTIALLY_COMPLETED,
                HabitPaintingTypes.COMPLETED
            ])
            .count()
        )

    class Meta:
        verbose_name = 'Привычка'
        verbose_name_plural = 'Привычки'


class HabitPainting(models.Model):
    habit = models.ForeignKey(
        Habit,
        verbose_name='Привычка',
        on_delete=models.CASCADE
    )
    type = models.CharField(
        verbose_name='Тип',
        max_length=31,
        choices=HabitPaintingTypes.choices
    )

    # If type is not 'pre_generated'
    date = models.DateField(
        verbose_name='Дата получения',
        **NULLABLE
    )

    # If type is 'completed'
    painting = models.ImageField(
        verbose_name='Картина',
        upload_to='paintings',
        **NULLABLE
    )

    # If type is 'completed' or 'pre_generated'
    painting_name = models.CharField(
        verbose_name='Название картины',
        max_length=255,
        **NULLABLE
    )


class HabitDeadlineCheck(models.Model):
    date = models.DateField(
        verbose_name='Дата проведения'
    )
    status = models.CharField(
        verbose_name='Статус',
        max_length=31,
        choices=HabitDeadlineCheckStatus.choices
    )
