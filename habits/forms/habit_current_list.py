from django import forms


class OrderHabitForm(forms.Form):
    order = forms.ChoiceField(
        label='Сортировка',
        choices=(('deadline', 'Deadline asc'), ('-deadline', 'Deadline desc'))
    )
