from django import template

from habits.choices import HabitTypes

register = template.Library()


@register.filter
def habit_type_to_rus_1(value: str):
    return {
        HabitTypes.DAILY.value: 'дневной',
        HabitTypes.WEEKLY.value: 'недельной',
        HabitTypes.MONTHLY.value: 'месячной'
    }.get(value)


@register.filter
def habit_type_to_rus_2(value: str):
    return {
        HabitTypes.DAILY.value: ' дня',
        HabitTypes.WEEKLY.value: 'недели',
        HabitTypes.MONTHLY.value: 'месяца'
    }.get(value)
