import base64
import calendar
import datetime
from uuid import uuid4

from django.core.files.base import ContentFile
from wonderwords import RandomSentence

from habit_gallery.settings import FUSION_AI_URL, FUSION_AI_API_KEY, FUSION_AI_SECRET_KEY
from habits.choices import HabitTypes
from habits.models import Habit
from habits.services import Text2ImageAPI


def bool_str_to_bool(bool_str: str) -> bool:
    return {'true': True, 'false': False}.get(bool_str)


def get_diff_weeks(d1: datetime.date, d2: datetime.date):
    monday1 = (d1 - datetime.timedelta(days=d1.weekday()))
    monday2 = (d2 - datetime.timedelta(days=d2.weekday()))
    return (monday2 - monday1).days / 7


def get_diff_months(d1: datetime.date, d2: datetime.date):
    return (d1.year - d2.year) * 12 + d1.month - d2.month


def is_day_frequency(habit: Habit, day: datetime.date):
    return (day - habit.start_date).days % habit.frequency == 0


def is_week_frequency(habit: Habit, day: datetime.date):
    return get_diff_weeks(day, habit.start_date) % habit.frequency == 0


def is_month_frequency(habit: Habit, day: datetime.date):
    return get_diff_months(day, habit.start_date) % habit.frequency == 0


def get_first_date_of_week(day: datetime.date) -> datetime.date:
    return day - datetime.timedelta(days=day.weekday())


def get_last_date_of_week(day: datetime.date) -> datetime.date:
    return get_first_date_of_week(day) + datetime.timedelta(days=6)


def get_first_date_of_month(day: datetime.date):
    return datetime.date(day.year, day.month, 1)


def get_last_date_of_month(day: datetime.date):
    return datetime.date(day.year, day.month, calendar.monthrange(day.year, day.month)[1])


def is_day_expected(habit: Habit, day: datetime.date) -> bool:
    return (
        (
            habit.type == HabitTypes.DAILY and
            is_day_frequency(habit, day)
        ) or
        (
            habit.type == HabitTypes.WEEKLY and
            is_week_frequency(habit, day) and
            (
                day.weekday() in habit.selected_days
                if habit.selected_days
                else day == get_last_date_of_week(day)
            )
        ) or
        (
            habit.type == HabitTypes.MONTHLY and
            is_month_frequency(habit, day) and
            (
                day.day in habit.selected_days
                if habit.selected_days
                else day == get_last_date_of_month(day)
            )
        )
    )


def is_last_selected_day(habit: Habit, day: datetime.date) -> tuple[bool | None, int | None]:
    if not habit.selected_days:
        return None, None
    current_day_info = None
    if habit.type == HabitTypes.WEEKLY:
        current_day_info = day.weekday()
    elif habit.type == HabitTypes.MONTHLY:
        current_day_info = day.day
    if current_day_info is None:
        return None, None
    current_part_index = sorted(habit.selected_days).index(current_day_info)
    return current_part_index == len(habit.selected_days) - 1, current_part_index


def get_deadline(habit: Habit, day: datetime.date) -> datetime.date | None:
    if (
        habit.type == HabitTypes.WEEKLY and
        not habit.selected_days and
        is_week_frequency(habit, day)
    ):
        return get_last_date_of_week(day)
    if (
        habit.type == HabitTypes.MONTHLY and
        not habit.selected_days and
        is_month_frequency(habit, day)
    ):
        return get_last_date_of_month(day)
    return day if is_day_expected(habit, day) else None


def generate_painting_name() -> str:
    s = RandomSentence()
    return s.sentence()[:-1]


def generate_painting_image(name: str, uuid_name: str = None) -> ContentFile:
    api = Text2ImageAPI(
        FUSION_AI_URL,
        FUSION_AI_API_KEY,
        FUSION_AI_SECRET_KEY
    )
    model_id = api.get_model()
    uuid = api.generate(
        name,
        model_id,
        style='Painting, classicism',
        width=1024,
        height=576
    )
    image = api.check_generation(uuid)[0]
    uuid_name = uuid_name or str(uuid4())
    return ContentFile(base64.b64decode(image), name=uuid_name + '.png')
