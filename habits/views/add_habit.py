from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils import timezone

from habits.choices import HabitTypes
from habits.forms import AddDailyHabitForm
from habits.forms.add_habit import (
    AddFlexibleWeeklyHabitForm, AddFixedWeeklyHabitForm,
    AddFlexibleMonthlyHabitForm, AddFixedMonthlyHabitForm,
)
from habits.models import Habit
from habits.tasks import pre_generate_painting
from habits.utils import bool_str_to_bool


@login_required(redirect_field_name='', login_url='/login/')
def add_habit_view(request):
    habit_type = request.GET.get('type', HabitTypes.DAILY)
    is_flexible = bool_str_to_bool(request.GET.get('flexible', 'true'))

    form_class = AddDailyHabitForm
    if habit_type == HabitTypes.WEEKLY:
        form_class = AddFlexibleWeeklyHabitForm if is_flexible else AddFixedWeeklyHabitForm
    elif habit_type == HabitTypes.MONTHLY:
        form_class = AddFlexibleMonthlyHabitForm if is_flexible else AddFixedMonthlyHabitForm

    form = form_class(initial={'type': habit_type})

    if request.method == 'POST':
        is_flexible = 'selected_days' not in request.POST
        form = form_class(request.POST, request=request)
        if form.is_valid():
            habit = Habit.objects.create(**form.cleaned_data)
            pre_generate_painting.delay(habit.pk)
            return redirect('habit-list')

    header_left_btn_url = reverse('habit-list')
    return render(request, 'habits/add_habit.html', locals())
