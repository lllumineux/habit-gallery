import datetime

from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse

from habits.forms import OrderHabitForm
from habits.models import Habit
from habits.tasks import pre_generate_painting
from habits.utils import get_deadline


@login_required(redirect_field_name='', login_url='/login/')
def habit_current_list_view(request):
    order_field = request.GET.get('order', 'deadline')
    order_form = OrderHabitForm({'order': order_field})
    if not order_form.is_valid():
        raise Http404

    if request.method == 'POST':
        h = get_object_or_404(Habit, pk=request.POST.get('habit_id'), author=request.user)
        if h.completed_amount >= h.current_required_amount:
            raise Http404
        h.completed_amount += 1
        h.save(update_fields=['completed_amount'])
        if h.completed_amount == h.current_required_amount:
            pre_generate_painting.delay(h.pk)
            h.current_streak += 1
            h.save(update_fields=['current_streak'])
            return redirect('habit-new-painting', pk=h.pk)
        return redirect(reverse('habit-current-list') + f'?order={order_field}')

    habits = []
    today = datetime.date.today()
    for habit in Habit.objects.filter(author=request.user):
        habit.deadline = get_deadline(habit, today)
        if habit.deadline:
            habits.append(habit)
    habits.sort(key=lambda x: (x.deadline, x.name), reverse=order_field[:1] == '-')

    return render(request, 'habits/habit_current_list.html', locals())
