from django.urls import path
from django.views.generic import RedirectView

from habits.views import (
    add_habit_view, habit_list_view, habit_detail_view,
    habit_current_list_view, habit_new_painting_view, habit_painting_detail_view, edit_habit_view,
    delete_habit_view,
)

urlpatterns = [
    path('', RedirectView.as_view(url='current/', permanent=False), name='main'),
    path('current/', habit_current_list_view, name='habit-current-list'),
    path('habits/', habit_list_view, name='habit-list'),
    path('habits/add/', add_habit_view, name='add-habit'),
    path('habits/<int:pk>/', habit_detail_view, name='habit-detail'),
    path('habits/<int:pk>/edit/', edit_habit_view, name='edit-habit'),
    path('habits/<int:pk>/delete/', delete_habit_view, name='delete-habit'),
    path('habits/<int:pk>/new_painting/', habit_new_painting_view, name='habit-new-painting'),
    path('painting/<int:pk>/', habit_painting_detail_view, name='habit-painting-detail')
]
