from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from accounts.forms import AccountNewEmailForm, AccountNewPasswordForm


@login_required(redirect_field_name='', login_url='/login/')
def account_detail_view(request):
    email = request.user.email
    return render(request, 'accounts/account_detail.html', locals())


@login_required(redirect_field_name='', login_url='/login/')
def edit_account_email_view(request):
    form = AccountNewEmailForm()
    if request.method == 'POST':
        form = AccountNewEmailForm(request.POST)
        if form.is_valid():
            request.user.email = form.cleaned_data.get('new_email')
            request.user.save(update_fields=['email'])
            redirect('account-detail')
    return render(request, 'accounts/edit_account_email.html', locals())


@login_required(redirect_field_name='', login_url='/login/')
def edit_account_password_view(request):
    form = AccountNewPasswordForm()
    if request.method == 'POST':
        form = AccountNewPasswordForm(request.POST)
        if form.is_valid():
            request.user.set_password(form.cleaned_data.get('new_password'))
            request.user.save(update_fields=['password'])
            redirect('account-detail')
    return render(request, 'accounts/edit_account_password.html', locals())
