from django.urls import path

from accounts.views import (
    signup_view, login_view, logout_view, account_detail_view,
    edit_account_email_view, edit_account_password_view,
)

urlpatterns = [
    path('signup/', signup_view, name='signup'),
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('account/', account_detail_view, name='account-detail'),
    path('account/edit/email/', edit_account_email_view, name='edit-account-email'),
    path('account/edit/password/', edit_account_password_view, name='edit-account-password')
]
