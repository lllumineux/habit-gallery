from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse

from habits.forms.edit_habit import EditHabitForm
from habits.models import Habit


@login_required(redirect_field_name='', login_url='/login/')
def edit_habit_view(request, pk=None):
    habit = get_object_or_404(Habit, pk=pk, author=request.user)
    form = EditHabitForm(initial={'name': habit.name})
    if request.method == 'POST':
        form = EditHabitForm(request.POST)
        if form.is_valid():
            habit.name = form.cleaned_data.get('name')
            habit.save(update_fields=['name'])
            return redirect('habit-detail', pk=pk)
    return render(request, 'habits/edit_habit.html', locals())
