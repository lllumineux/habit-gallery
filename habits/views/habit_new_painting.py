import datetime

from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.shortcuts import render, get_object_or_404

from habits.choices import HabitPaintingTypes, HabitTypes
from habits.models import HabitPainting, Habit
from habits.utils import is_last_selected_day


@login_required(redirect_field_name='', login_url='/login/')
def habit_new_painting_view(request, pk=None):
    today = datetime.date.today()
    habit = get_object_or_404(Habit, pk=pk, author=request.user)

    new_painting = (
        HabitPainting.objects
        .filter(habit=habit, type=HabitPaintingTypes.PRE_GENERATED)
        .order_by('id')
        .first()
    )

    if (
        not new_painting or
        habit.completed_amount != habit.current_required_amount or
        HabitPainting.objects.filter(habit=habit, date=today).exists()
    ):
        raise Http404

    new_painting.date = today
    new_painting.type = HabitPaintingTypes.COMPLETED
    is_last_part, current_part_index = is_last_selected_day(habit, today)
    if is_last_part is False:
        new_painting.type = HabitPaintingTypes.PARTIALLY_COMPLETED

    new_painting.save(update_fields=['date', 'type'])

    return render(request, 'habits/habit_new_painting.html', locals())
