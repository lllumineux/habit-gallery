function showPassword() {
  document.querySelector('#show-password').style.display = 'none';
  document.querySelector('#hide-password').style.display = 'block';
  document.querySelector('input[name="password"]').type = 'text';
}

function hidePassword() {
  document.querySelector('#hide-password').style.display = 'none';
  document.querySelector('#show-password').style.display = 'block';
  document.querySelector('input[name="password"]').type = 'password';
}

function decreaseNumber(obj) {
  const input = obj.parentElement.querySelector('input[type="number"]');
  if (input.value === '') {
    input.value = 0;
  } else if (input.value <= 0) {
    input.value = 0;
  } else {
    input.value = parseInt(input.value) - 1;
  }

  if (input.value === '0') {
    obj.style.opacity = '.25';
  }
}

function increaseNumber(obj) {
  const parent = obj.parentElement;
  const input = parent.querySelector('input[type="number"]');
  if (input.value === '') {
    input.value = 0;
  } else {
    input.value = parseInt(input.value) + 1;
  }

  if (input.value > 0) {
    const decrease = parent.querySelector('#decrease-number');
    decrease.style.opacity = '1';
  }
}

const select = document.querySelector('.input > .input__field > select');
if (select) {
  select.addEventListener('mousedown', (e) => {
    e.preventDefault();
    const option = e.target;
    if (option.hasAttribute('selected')) {
      option.removeAttribute('selected');
    } else {
      option.setAttribute('selected', '');
    }
  })
}
