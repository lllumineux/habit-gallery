from .add_habit import add_habit_view
from .delete_habit import delete_habit_view
from .edit_habit import edit_habit_view
from .habit_current_list import habit_current_list_view
from .habit_detail import habit_detail_view
from .habit_list import habit_list_view
from .habit_new_painting import habit_new_painting_view
from .habit_painting_detail import habit_painting_detail_view
