from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.forms import forms, fields, PasswordInput, TextInput

from accounts.models import User


class CustomForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.label_suffix = ''


class SignupForm(CustomForm):
    email = fields.EmailField(
        label='Email',
        max_length=32,
        widget=TextInput(attrs={'placeholder': 'Введите email'})
    )
    password = fields.CharField(
        label='Пароль',
        max_length=32,
        widget=PasswordInput(attrs={'placeholder': 'Введите пароль'}),
        validators=[validate_password]
    )

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise ValidationError('Пользователь с таким email уже существует.')
        return email


class LoginForm(CustomForm):
    email = fields.EmailField(
        label='Email',
        max_length=32,
        widget=TextInput(attrs={'placeholder': 'Введите email'})
    )
    password = fields.CharField(
        label='Пароль',
        max_length=32,
        widget=PasswordInput(attrs={'placeholder': 'Введите пароль'})
    )


class AccountNewEmailForm(CustomForm):
    new_email = fields.EmailField(
        label='Новый email',
        max_length=32,
        widget=TextInput(attrs={'placeholder': 'Введите новый email'}),
    )


class AccountNewPasswordForm(CustomForm):
    new_password = fields.CharField(
        label='Новый пароль',
        max_length=32,
        widget=PasswordInput(attrs={'placeholder': 'Введите новый пароль'}),
        validators=[validate_password]
    )
