from django import forms


class DateInput(forms.DateInput):
    input_type = 'date'


class DateField(forms.DateField):
    widget = DateInput
