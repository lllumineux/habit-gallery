from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404

from habits.models import HabitPainting


@login_required(redirect_field_name='', login_url='/login/')
def habit_painting_detail_view(request, pk=None):
    painting = get_object_or_404(HabitPainting, pk=pk, habit__author=request.user)
    return render(request, 'habits/habit_painting_detail.html', locals())
