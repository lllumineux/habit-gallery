from typing import Callable

from django.core.handlers.wsgi import WSGIRequest
from django.shortcuts import redirect

from accounts.models import User


def create_user(email: str, password: str) -> User:
    user = User(username=email, email=email)
    user.set_password(password)
    user.save()
    return user


def not_authenticated_required(func: Callable) -> Callable:
    def wrapper(request: WSGIRequest, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('add-habit')
        return func(request, *args, **kwargs)
    return wrapper
