from django.db.models import TextChoices, IntegerChoices


class HabitTypes(TextChoices):
    DAILY = 'daily', 'Дневная'
    WEEKLY = 'weekly', 'Недельная'
    MONTHLY = 'monthly', 'Месячная'


class HabitPaintingTypes(TextChoices):
    PRE_GENERATED = 'pre_generated', 'Заранее сгенерированная'
    PARTIALLY_COMPLETED = 'partially_completed', 'Частично завершённая'
    COMPLETED = 'completed', 'Завершённая'
    MISSED = 'missed', 'Пропущенная'


class HabitCalendarDayTypes(TextChoices):
    NONE = 'none', 'Не относится к месяцу'
    EMPTY = 'empty', 'Ничего не произошло'
    PARTIALLY_COMPLETED = 'partially_completed', 'Получена часть картины'
    COMPLETED = 'completed', 'Получена целая картина'
    MISSED = 'missed', 'Картина пропущена'
    EXPECTED_PART = 'expected_part', 'Ожидается получение части картины'
    EXPECTED_FULL = 'expected_full', 'Ожидается получение картины целиком'


class WeekdayChoices(IntegerChoices):
    MONDAY = 0, 'Понедельник'
    TUESDAY = 1, 'Вторник'
    WEDNESDAY = 2, 'Среда'
    THURSDAY = 3, 'Четверг'
    FRIDAY = 4, 'Пятница'
    SATURDAY = 5, 'Суббота'
    SUNDAY = 6, 'Воскресенье'


class HabitDeadlineCheckStatus(TextChoices):
    STARTED = 'started', 'Началась'
    FINISHED = 'finished', 'Закончилась'
