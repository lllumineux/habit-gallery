from dataclasses import dataclass

from habits.choices import HabitCalendarDayTypes
from habits.models import HabitPainting


@dataclass
class HabitCalendarDay:
    type: HabitCalendarDayTypes
    num: int = None
    painting: HabitPainting = None
