from django import forms


class CalendarHabitForm(forms.Form):
    calendar_date = forms.DateField()
