from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from habits.choices import HabitTypes
from habits.forms import SearchHabitForm
from habits.models import Habit


@login_required(redirect_field_name='', login_url='/login/')
def habit_list_view(request):
    search_form = SearchHabitForm(request.GET)
    search_term = search_form.cleaned_data.get('search') if search_form.is_valid() else None

    habits_with_type = []
    for value, label in HabitTypes.choices:
        habits_with_type.append((
            label.replace('ая', 'ые'),
            Habit.objects.filter(
                author=request.user,
                type=value,
                **({'name__icontains': search_term} if search_term else {})
            ).order_by('name')
        ))

    return render(request, 'habits/habit_list.html', locals())
