from django import forms

from habits.models import Habit


class EditHabitForm(forms.ModelForm):
    name = forms.CharField(
        label='Новое название',
        widget=forms.TextInput(attrs={'placeholder': 'Введите новое название'})
    )

    class Meta:
        model = Habit
        fields = ('name',)
