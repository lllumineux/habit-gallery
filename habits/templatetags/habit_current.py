import datetime

from dateutil import relativedelta
from django import template

register = template.Library()


@register.filter
def reverse_order(value: str):
    return value[1:] if value[:1] == '-' else f'-{value}'
