import datetime

from dateutil import relativedelta
from django import template

register = template.Library()


@register.filter
def prev_month(value: datetime.date):
    return value - relativedelta.relativedelta(months=1)


@register.filter
def next_month(value: datetime.date):
    return value + relativedelta.relativedelta(months=1)


@register.simple_tag
def get_date(year, month, day):
    return datetime.date(year, month, day)
