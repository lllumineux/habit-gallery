import calendar
import datetime

from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django.urls import reverse

from habits.choices import HabitCalendarDayTypes, HabitTypes, HabitPaintingTypes
from habits.dataclasses import HabitCalendarDay
from habits.forms import CalendarHabitForm
from habits.models import Habit, HabitPainting
from habits.utils import (
    is_day_expected, get_first_date_of_week,
    get_last_date_of_week, get_first_date_of_month, get_last_date_of_month, is_last_selected_day,
)


@login_required(redirect_field_name='', login_url='/login/')
def habit_detail_view(request, pk=None):
    today = datetime.date.today()
    calendar_date = request.GET.get('calendar_date', today.strftime('%Y-%m-%d'))
    calendar_form = CalendarHabitForm({'calendar_date': calendar_date})
    if not calendar_form.is_valid():
        raise Http404
    calendar_date = calendar_form.cleaned_data.get('calendar_date')

    habit = get_object_or_404(Habit, pk=pk, author=request.user)

    calendar_rows = []
    for days_row in calendar.monthcalendar(calendar_date.year, calendar_date.month):
        row = []
        for day in days_row:
            # If day is not in a current month
            if day == 0:
                row.append(HabitCalendarDay(type=HabitCalendarDayTypes.NONE))
                continue

            current_date = datetime.date(calendar_date.year, calendar_date.month, day)

            # If day has a painting (partially completed, completed or missed)
            if current_date <= today:
                painting = HabitPainting.objects.filter(
                    ~Q(type=HabitPaintingTypes.PRE_GENERATED),
                    habit=habit,
                    date=current_date
                ).first()
                if painting:
                    row.append(HabitCalendarDay(
                        type=painting.type,
                        num=day,
                        painting=painting
                    ))
                    continue
                elif not painting and current_date != today:
                    row.append(HabitCalendarDay(
                        type=HabitCalendarDayTypes.EMPTY,
                        num=day
                    ))
                    continue

            # If day is expected to have a painting in the future
            if current_date >= today:
                # If days of the week/month are specified
                if habit.selected_days:
                    if is_day_expected(habit, current_date):
                        # If last day of the week/month (gets completed painting)
                        if is_last_selected_day(habit, current_date)[0]:
                            row.append(HabitCalendarDay(
                                type=HabitCalendarDayTypes.EXPECTED_FULL,
                                num=day
                            ))
                            continue
                        # If not last day of the week/month (gets part of the painting)
                        else:
                            row.append(HabitCalendarDay(
                                type=HabitCalendarDayTypes.EXPECTED_PART,
                                num=day
                            ))
                            continue

                # If days of the week/month are not specified
                else:
                    date_gte, date_lte = None, None
                    if habit.type == HabitTypes.WEEKLY:
                        date_gte = get_first_date_of_week(current_date)
                        date_lte = get_last_date_of_week(current_date)
                    elif habit.type == HabitTypes.MONTHLY:
                        date_gte = get_first_date_of_month(current_date)
                        date_lte = get_last_date_of_month(current_date)
                    # If painting has been already given that week/month previously
                    if (
                        date_gte and date_lte and
                        HabitPainting.objects
                        .filter(habit=habit, date__gte=date_gte, date__lte=date_lte)
                        .exists()
                    ):
                        row.append(HabitCalendarDay(
                            type=HabitCalendarDayTypes.EMPTY,
                            num=day,
                        ))
                        continue
                    if is_day_expected(habit, current_date):
                        row.append(HabitCalendarDay(
                            type=HabitCalendarDayTypes.EXPECTED_FULL,
                            num=day,
                        ))
                        continue

            # If day is in a current month, but doesn't have any features
            row.append(HabitCalendarDay(
                type=HabitCalendarDayTypes.EMPTY,
                num=day,
            ))

        calendar_rows.append(row)

    header_left_btn_url = reverse('habit-list')
    header_right_btn_type = 'edit_habit'
    header_right_btn_url = reverse('edit-habit', args=(pk,))
    return render(request, 'habits/habit_detail.html', locals())
