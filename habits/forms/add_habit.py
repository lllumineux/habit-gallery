from django import forms
from django.core.exceptions import ValidationError
from django.utils import timezone

from habits.choices import WeekdayChoices
from habits.forms.fields import DateField
from habits.models import Habit


class AddHabitBase(forms.ModelForm):
    name = forms.CharField(
        label='Название',
        widget=forms.TextInput(attrs={'placeholder': 'Введите название'})
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super().__init__(*args, **kwargs)

    def clean(self):
        self.cleaned_data['author'] = self.request.user

    def clean_start_date(self):
        start_date = self.cleaned_data.get('start_date')
        if start_date < timezone.now().date():
            raise ValidationError('Дата начала должна быть больше текущей даты.')
        return start_date


class AddFlexibleHabitBase(AddHabitBase):
    start_date = DateField(label=Habit._meta.get_field('start_date').verbose_name)

    class Meta:
        model = Habit
        fields = (
            'type',
            'name',
            'start_date',
            'frequency',
            'required_amount'
        )


class AddFixedHabitBase(AddHabitBase):
    start_date = DateField(label=Habit._meta.get_field('start_date').verbose_name)
    selected_days = forms.MultipleChoiceField()

    def clean_selected_days(self):
        selected_days = self.cleaned_data.get('selected_days', [])
        return [int(x) for x in selected_days]

    class Meta:
        model = Habit
        fields = (
            'type',
            'name',
            'start_date',
            'frequency',
            'selected_days',
            'required_amount_per_day'
        )


class AddDailyHabitForm(AddFlexibleHabitBase):
    class Meta(AddFlexibleHabitBase.Meta):
        pass


class AddFlexibleWeeklyHabitForm(AddFlexibleHabitBase):
    class Meta(AddFlexibleHabitBase.Meta):
        pass


class AddFixedWeeklyHabitForm(AddFixedHabitBase):
    selected_days = forms.MultipleChoiceField(
        label=Habit._meta.get_field('selected_days').verbose_name,
        choices=[(v, n[0]) for v, n in WeekdayChoices.choices]
    )

    class Meta(AddFixedHabitBase.Meta):
        pass


class AddFlexibleMonthlyHabitForm(AddFlexibleHabitBase):
    class Meta(AddFlexibleHabitBase.Meta):
        pass


class AddFixedMonthlyHabitForm(AddFixedHabitBase):
    selected_days = forms.MultipleChoiceField(
        label=Habit._meta.get_field('selected_days').verbose_name,
        choices=[(i, i) for i in range(1, 32)]
    )

    class Meta(AddFixedHabitBase.Meta):
        pass
