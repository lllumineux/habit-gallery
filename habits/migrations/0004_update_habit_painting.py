# Generated by Django 5.0.4 on 2024-04-22 12:36

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("habits", "0003_add_habit_completed_amount"),
    ]

    operations = [
        migrations.AddField(
            model_name="habitpainting",
            name="painting_name",
            field=models.CharField(
                blank=True, max_length=255, null=True, verbose_name="Название картины"
            ),
        ),
        migrations.AlterField(
            model_name="habit",
            name="type",
            field=models.CharField(
                choices=[
                    ("daily", "Дневная"),
                    ("weekly", "Недельная"),
                    ("monthly", "Месячная"),
                ],
                default="daily",
                max_length=15,
                verbose_name="Тип",
            ),
        ),
        migrations.AlterField(
            model_name="habitpainting",
            name="date",
            field=models.DateField(
                blank=True, null=True, verbose_name="Дата получения"
            ),
        ),
        migrations.AlterField(
            model_name="habitpainting",
            name="painting",
            field=models.ImageField(
                blank=True, null=True, upload_to="paintings", verbose_name="Картина"
            ),
        ),
        migrations.AlterField(
            model_name="habitpainting",
            name="type",
            field=models.CharField(
                choices=[
                    ("pre_generated", "Заранее сгенерированная"),
                    ("partially_completed", "Частично завершённая"),
                    ("completed", "Завершённая"),
                    ("missed", "Пропущенная"),
                ],
                max_length=31,
                verbose_name="Тип",
            ),
        ),
    ]
