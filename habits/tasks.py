import datetime

from django.shortcuts import get_object_or_404

from habit_gallery.celery import app
from habits.choices import HabitPaintingTypes, HabitDeadlineCheckStatus
from habits.models import Habit, HabitPainting, HabitDeadlineCheck
from habits.utils import generate_painting_image, generate_painting_name, get_deadline


@app.task(bind=True)
def pre_generate_painting(self, habit_id):
    habit = get_object_or_404(Habit, pk=habit_id)
    painting_name = generate_painting_name()
    painting = HabitPainting.objects.create(
        habit=habit,
        type=HabitPaintingTypes.PRE_GENERATED,
        painting_name=painting_name
    )
    painting_image = generate_painting_image(painting_name, uuid_name=self.request.id)
    painting.painting = painting_image
    painting.save(update_fields=['painting'])


@app.task
def check_and_update_deadlines():
    last_check = HabitDeadlineCheck.objects.order_by('-date').first()
    current_check_date = last_check.date + datetime.timedelta(days=1)
    while current_check_date < datetime.date.today():
        current_check = HabitDeadlineCheck.objects.create(
            date=current_check_date,
            status=HabitDeadlineCheckStatus.STARTED
        )
        habits_to_update = []
        for habit in Habit.objects.iterator():
            habit_deadline = get_deadline(habit, current_check_date)
            if habit_deadline and habit_deadline == current_check_date:
                has_completed_paintings = (
                    HabitPainting.objects
                    .filter(
                        habit=habit,
                        date=current_check_date,
                        type__in=[
                            HabitPaintingTypes.PARTIALLY_COMPLETED,
                            HabitPaintingTypes.COMPLETED
                        ]
                    ).exists()
                )
                if not has_completed_paintings:
                    HabitPainting.objects.create(
                        habit=habit,
                        type=HabitPaintingTypes.MISSED,
                        date=current_check_date
                    )
                    habit.current_streak = 0
            habit.completed_amount = 0
            habits_to_update.append(habit)

        Habit.objects.bulk_update(habits_to_update, fields=('completed_amount', 'current_streak'))
        current_check.status = HabitDeadlineCheckStatus.FINISHED
        current_check.save()
        current_check_date += datetime.timedelta(days=1)
