from django.core.handlers.wsgi import WSGIRequest


def common_tags(request: WSGIRequest):
    header_type = None
    if (
        '/habits/' in request.path and
        request.path != '/habits/' and
        'new_painting' not in request.path
    ):
        header_type = 'habits'

    footer_type = None
    if '/current/' == request.path:
        footer_type = 'current'
    elif (
        '/habits/' in request.path and
        'new_painting' not in request.path
    ):
        footer_type = 'habits'
    elif '/account/' == request.path:
        footer_type = 'settings'

    return {
        'header_type': header_type,
        'footer_type': footer_type
    }
